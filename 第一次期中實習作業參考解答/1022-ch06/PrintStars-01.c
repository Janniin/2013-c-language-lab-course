#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, j;
    for (i=1; i<=7; i+=2) {    //  this loop to control line
        for (j=1; j<=7-i; j++)  //  this loop to control space printing
            printf(" ");
        for (j=1; j<=i; j++)   //  this loop to control star printing
            printf("*");
        printf("\n");          //  new line
    }
    return 0;
}
