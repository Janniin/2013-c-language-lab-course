#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, j;

    for (i=1; i<=4; i++) {             // this loop to control line
        for (j=1; j<=7-(2*i-1); j++)   // this loop to control spaces printing
            printf(" ");
        for (j=1; j<=2*i-1; j++)       // this loop to control star printing
            printf("*");
        printf("\n"); // new line
    }

    return 0;
}
