// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int numerator, denominator;  // numerator為分子, denominator為分母
    int n, m, remainder;  // n為被除數, m為除數, remainder為餘數

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d/%d", &numerator, &denominator);

    // Step 3:
    // Computation area
    // Write down your math in here
    n = numerator;
    m = denominator;
    while(1) {
        remainder = n % m;
        n = m;
        m = remainder;
        if (m == 0)
            break;
    }

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d/%d\n", numerator/n, denominator/n);

    return 0;
}
