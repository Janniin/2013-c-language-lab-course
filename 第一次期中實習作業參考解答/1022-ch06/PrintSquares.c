// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int n, m, i;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d, %d", &n, &m);

    // Step 3:
    // Computation area
    // Write down your math in here

    // Step 4:
    // Output area
    // You use printf() in here
    for (i=2; i*i<=m; i+=2) {
        if (i*i>=n)
            printf("%03d\n", i*i);
    }

    return 0;
}
