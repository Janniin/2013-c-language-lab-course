// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int num1, num2, num3, num4, num5, num6, num7, num8, num9, num10, num11, num12, num13; // num%% is used to contain the EAN numbers
    int i, sum1=0, sum2=0, total=0, remainder; // i is used for count

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d",
          &num1, &num2, &num3, &num4, &num5, &num6, &num7, &num8, &num9, &num10, &num11, &num12, &num13);

    // Step 3:
    // Computation area
    // Write down your math in here

    sum1 = num2 + num4 + num6 + num8 + num10 + num10;  // sum1 is the sum of even digit numbers
    sum2 = num1 + num3 + num5 + num7 + num9 + num11;   // sum2 is the sum of odd digit numbers excluded the thirteenth digit

    total = 3*sum1 + sum2 - 1;
    remainder = total%10;
    remainder = 9 - remainder;

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d\n", remainder);

    return 0;
}
