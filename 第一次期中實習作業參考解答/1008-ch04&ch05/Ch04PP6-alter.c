// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int i, num[13], sum1=0, sum2=0, total=0, remainder; // i is used for count, num is used to contain the EAN numbers

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    for (i=0; i<13; i++) {
        scanf("%1d", &num[i]);
    }

    // Step 3:
    // Computation area
    // Write down your math in here
    for (i=1; i<12; i+=2)
        sum1 += num[i];
    for (i=0; i<12; i+=2)
        sum2 += num[i];
    total = 3*sum1 + sum2 - 1;
    remainder = total%10;
    remainder = 9 - remainder;

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d\n", remainder);

    return 0;
}
