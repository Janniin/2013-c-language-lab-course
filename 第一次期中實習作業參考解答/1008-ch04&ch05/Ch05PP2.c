// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int hour, minute;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d:%d", &hour, &minute);

    // Step 3:
    // Computation area
    // Write down your math in here

    // Step 4:
    // Output area
    // You use printf() in here
    if (hour == 0)
        printf("%02d:%02d AM\n", hour, minute);
    else if (hour == 24)
        printf("%02d:%02d AM\n", hour-24, minute);
    else if (hour == 12)
        printf("%02d:%02d PM\n", hour, minute);
    else if (hour > 12)
        printf("%02d:%02d PM\n", hour-12, minute);

    return 0;
}
