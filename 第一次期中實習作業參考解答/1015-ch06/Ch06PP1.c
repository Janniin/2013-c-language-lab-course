// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int n, maxNum = 0;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input

    // Step 3:
    // Computation area
    // Write down your math in here
    while(1) {
        scanf("%d", &n);
        if (n > maxNum)
            maxNum = n;
        if (n <= 0)
            break;
    }

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d\n", maxNum);
    return 0;


    return 0;
}
