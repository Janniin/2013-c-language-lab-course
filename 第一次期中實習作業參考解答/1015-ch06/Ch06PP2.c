// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int n, m, remainder;  // n為被除數, m為除數, remainder為餘數

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d", &n);
    scanf("%d", &m);

    // Step 3:
    // Computation area
    // Write down your math in here
    while(1) {
        remainder = n % m;
        n = m;
        m = remainder;
        if (m == 0)
            break;
    }

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d\n", n);

    return 0;
}
