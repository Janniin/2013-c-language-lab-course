// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int f1, f2, g1, g2, r1, r2;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d/%d+%d/%d", &f1, &f2, &g1, &g2);

    // Step 3:
    // Computation area
    // Write down your math in here
    r1 = f1*g2+f2*g1;
    r2 = f2*g2;

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d/%d\n", r1, r2);

    return 0;
}
