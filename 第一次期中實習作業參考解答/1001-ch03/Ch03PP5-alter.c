// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int num[16], i;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    for (i=0; i<16; i++)
        scanf("%d", &num[i]);

    // Step 3:
    // Computation area
    // Write down your math in here

    // Step 4:
    // Output area
    // You use printf() in here
    for (i=0; i<16; i++) {
        printf("%2d", num[i]);
        if ((i+1)%4 == 0)
            printf("\n");
        else
            printf(" ");
    }

    return 0;
}
