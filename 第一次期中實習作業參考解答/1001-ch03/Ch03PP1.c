// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int year = 0, month = 0, day = 0;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d/%d/%d", &month, &day ,&year);

    // Step 3:
    // Computation area
    // Write down your math in here

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d%02d%02d\n", year, month, day);

    return 0;
}
