// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int num1, num2, num3, num4, num5, num6, num7, num8, num9, num10, num11, num12, num13, num14, num15, num16, i;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", &num1, &num2, &num3, &num4, &num5, &num6,
          &num7, &num8, &num9, &num10, &num11, &num12, &num13, &num14, &num15, &num16);

    // Step 3:
    // Computation area
    // Write down your math in here

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%2d%3d%3d%3d\n%2d%3d%3d%3d\n%2d%3d%3d%3d\n%2d%3d%3d%3d\n",
           num1, num2, num3, num4, num5, num6, num7, num8, num9, num10, num11, num12, num13, num14, num15, num16);

    return 0;
}
