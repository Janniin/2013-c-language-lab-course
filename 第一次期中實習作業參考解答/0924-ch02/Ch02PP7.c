// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int b20, b10, b5, b1;  // represent the bill for $20, $10, $5, and $1
    int money;  // represent for total amount of money

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d", &money);

    // Step 3:
    // Computation area
    // Write down your math in here
    b20 = money / 20;
    b10 = (money - 20*b20) / 10;
    b5 = (money - 20*b20 - 10*b10) / 5;
    b1 = (money - 20*b20 - 10*b10 - 5*b5) / 1;

    // Step 4:
    // Output area
    // You use printf() in here
    printf("$20 bills: %d\n", b20);
    printf("$10 bills: %d\n", b10);
    printf(" $5 bills: %d\n", b5);
    printf(" $1 bills: %d\n", b1);

    return 0;
}
