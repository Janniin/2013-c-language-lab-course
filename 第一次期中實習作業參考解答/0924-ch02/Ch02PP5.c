// Directive area
#include <stdio.h>

int main(void)
{
    // Step1:
    // Declaration area
    // Write down your local variable declaration in here
    int x, res;

    // Step 2:
    // User interface area
    // You use printf() in here to prompt for input
    // And, use scanf() in here to read input
    scanf("%d", &x);

    // Step 3:
    // Computation area
    // Write down your math in here
    res = 3*x*x*x*x*x + 2*x*x*x*x - 5*x*x*x - x*x + 7*x - 6;

    // Step 4:
    // Output area
    // You use printf() in here
    printf("%d\n", res);

    return 0;
}
