#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

void read_word(int counts[26]);
bool equal_array(int counts1[26], int counts2[26]);

int main()
{
    int recordA[26]={0}, recordB[26]={0};

    read_word(recordA);
    read_word(recordB);

    if (equal_array(recordA, recordB))
        printf("The words are anagrams.\n");
    else
        printf("The words are not anagrams.\n");

    return 0;
}

void read_word(int counts[26])
{
    char ch;
    while ((ch = getchar()) != '\n') {
        if (isalpha(ch)) {
            ch = tolower(ch);
            counts[ch-'a']++;
        }
    }
}

bool equal_array(int counts1[26], int counts2[26])
{
    int i;
    for (i=0; i<26; i++) {
        if (counts1[i] != counts2[i])
            return false;  // end program here
    }

    return true;
}
