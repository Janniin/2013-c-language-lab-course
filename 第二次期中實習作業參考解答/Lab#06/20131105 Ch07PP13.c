// Directive area
#include <stdio.h>

int main(void)
{
    float average;
    int letter=0, word=1;
    char c;

    while ((c = getchar()) != '\n') {
        if (c == ' ')
            word++;
        else
            letter++;
    }
    average = (float)letter / word;
    printf("%.1f\n", average);

    return 0;
}
