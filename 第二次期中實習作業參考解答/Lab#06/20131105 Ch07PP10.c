// Directive area
#include <stdio.h>

int main(void)
{
    int n=0;
    char c;

    do {
        c = getchar();
        switch (c) {
            case 'A':
            case 'a':
            case 'E':
            case 'e':
            case 'I':
            case 'i':
            case 'O':
            case 'o':
            case 'U':
            case 'u':
                n++;
                break;
        }
    } while (c != '\n');
    printf("%d\n", n);

    return 0;
}
