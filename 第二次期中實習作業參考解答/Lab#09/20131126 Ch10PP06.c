#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

#define STACK_SIZE 100

/* external variables */
int contents[STACK_SIZE];
int top = 0;


void stack_overflow()
{
    printf("Stack is full, program terminates.\n");
    exit(EXIT_FAILURE);
}

void stack_underflow()
{
    printf("Stack is empty, program terminates.\n");
    exit(EXIT_FAILURE);
}

void make_empty(void)
{
    top = 0;
}

bool is_empty(void)
{
    return top == 0;
}

bool is_full(void)
{
    return top == STACK_SIZE;
}

void push(int i)
{
    if (is_full())
        stack_overflow();
    else
        contents[top++] = i;
}

int pop(void)
{
    if (is_empty())
        stack_underflow();
    else
        return contents[--top];

    return '\0'; /* prevents compiler warning due to stack_underflow() call */
}

int main()
{
    char ch;
    int num1, num2, result;
    while(1) {
        scanf(" %c", &ch);
        if (isdigit(ch))
            push((int)ch-'0');
        else if (ch == '+') {
            num2 = pop();
            num1 = pop();
            push(num1+num2);
        }
        else if (ch == '-') {
            num2 = pop();
            num1 = pop();
            push(num1-num2);
        }
        else if (ch == '*') {
            num2 = pop();
            num1 = pop();
            push(num1*num2);
        }
        else if (ch == '/') {
            num2 = pop();
            num1 = pop();
            push(num1/num2);
        }
        else if (ch == '=') {
            result = pop();
            if (is_empty()) {
                printf("%d\n", result);
                break;
            }
        }
    }

    return 0;
}
