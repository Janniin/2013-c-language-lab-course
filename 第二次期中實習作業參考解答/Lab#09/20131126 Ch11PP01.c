#include <stdio.h>

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones);

int main(void)
{
    int b20, b10, b5, b1;
    int money;

    scanf("%d", &money);

    pay_amount(money, &b20, &b10, &b5, &b1);

    printf("$20 bills: %d\n", b20);
    printf("$10 bills: %d\n", b10);
    printf(" $5 bills: %d\n", b5);
    printf(" $1 bills: %d\n", b1);

    return 0;
}

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones)
{
    *twenties = dollars / 20;
    *tens = (dollars - 20**twenties) / 10;
    *fives = (dollars - 20**twenties - 10**tens) / 5;
    *ones = (dollars - 20**twenties - 10**tens - 5**fives) / 1;
}
