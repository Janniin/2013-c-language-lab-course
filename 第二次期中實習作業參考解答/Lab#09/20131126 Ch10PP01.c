#include <stdio.h>
#include <stdbool.h>

#define STACK_SIZE 100

/* external variables */
char contents[STACK_SIZE];
int top = 0;

void make_empty(void)
{
    top = 0;
}

bool is_empty(void)
{
    return top == 0;
}

bool is_full(void)
{
    return top == STACK_SIZE;
}

void push(char ch)
{
    if (is_full())
        stack_overflow();
    else
        contents[top++] = ch;
}

char pop(void)
{
    if (is_empty())
        stack_underflow();
    else
        return contents[--top];
}

void stack_overflow()
{
    return 1;
}

void stack_underflow()
{
    return 1;
}

int main()
{
    bool properly_nested = true;
    char c;
    while (properly_nested && (c = getchar()) != '\n') {
        if (c == '(' || c == '{')  // if input is ( or {, push into stack
            push(c);
        else if (c == ')') // if input is ), pop out stack and check
            properly_nested = !is_empty() && pop() == '(';
        else if (c == '}') // if input is }, pop out stack and check
            properly_nested = !is_empty() && pop() == '{';
    }

    if (properly_nested && is_empty())
        printf("Parentheses/braces are nested properly\n");
    else
        printf("Parentheses/braces aren't nested properly\n");

    return 0;
}
