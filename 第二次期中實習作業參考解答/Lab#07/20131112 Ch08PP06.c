#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    char str[100], ch;
    int n, i;
    for (n=0; n<100; n++) {
        if ((ch = getchar()) == '\n')
            break;
        str[n] = toupper(ch);
    }

    for (i=0; i<n; i++) {
        switch (str[i]) {
            case 'A':
                putchar('4');
                break;
            case 'B':
                putchar('8');
                break;
            case 'E':
                putchar('3');
                break;
            case 'I':
                putchar('1');
                break;
            case 'O':
                putchar('0');
                break;
            case 'S':
                putchar('5');
                break;
            default:
                putchar(str[i]);
        }
    }
    printf("\n");
    return 0;
}
