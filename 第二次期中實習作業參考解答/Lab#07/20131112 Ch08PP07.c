#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, j, sum, arr[26];  // I'm gonna using this array from index 1, so there will be 26 elements in this array.

    for (i=1; i<=25; i++)
        scanf("%d", &arr[i]);

    printf("%d ", arr[1]+arr[2]+arr[3]+arr[4]+arr[5]);
    printf("%d ", arr[6]+arr[7]+arr[8]+arr[9]+arr[10]);
    printf("%d ", arr[11]+arr[12]+arr[13]+arr[14]+arr[15]);
    printf("%d ", arr[16]+arr[17]+arr[18]+arr[19]+arr[20]);
    printf("%d\n", arr[21]+arr[22]+arr[23]+arr[24]+arr[25]);

    printf("%d ", arr[1]+arr[6]+arr[11]+arr[16]+arr[21]);
    printf("%d ", arr[2]+arr[7]+arr[12]+arr[17]+arr[22]);
    printf("%d ", arr[3]+arr[8]+arr[13]+arr[18]+arr[23]);
    printf("%d ", arr[4]+arr[9]+arr[14]+arr[19]+arr[24]);
    printf("%d\n", arr[5]+arr[10]+arr[15]+arr[20]+arr[25]);

    /*for (i=1; i<=25; i+=5) {
        sum = 0;
        for (j=i; j<i+5; j++) {
            sum += arr[j];
        }
        printf("%d", sum);
        if (j>=25)
            printf("\n");
        else {
            //printf("j: %d", j);
            printf(" ");
        }
    }

    for (i=1; i<=25; i++) {
        sum = 0;
        for (j=i; j<=20+i; j+=5) {
            sum += arr[j];
        }
        printf("%d", sum);
        if (j>=25)
            printf("\n");
        else
            printf(" ");
    }*/

    return 0;
}
