#include <stdio.h>

int main()
{
    char arr[100];
    int i, n, strLen;  // strLen is used to record length of the input string

    // get user input and check input won't be '\n'
    for (strLen=0; (arr[strLen] = getchar()) != '\n'; strLen++);
    scanf("%d", &n);  // get shift number

    for (i=0; i<strLen; i++) {
        if (arr[i] >= 'A' && arr[i] <= 'Z')
            putchar((arr[i] - 'A' + n)%26 + 'A');
        else if (arr[i] >= 'a' && arr[i] <= 'z')
            putchar((arr[i] - 'a' + n)%26 + 'a');
        else putchar(arr[i]);
    }
    putchar('\n');

    return 0;
}
