#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, n=0, tempLen;
    char ch, str[150];

    while((ch = getchar()) != '\n') {
        if (ch == 'd') {
            i=0;
            tempLen = n;
            while(i<tempLen)
                str[n++] = str[i++];
        }

        else
            str[n++] = ch;
    }

    for(i=0; i<=n; i++)
        printf("%c", str[i]);
    printf("\n");

    return 0;
}
