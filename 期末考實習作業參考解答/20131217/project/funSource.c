#include "fun.h"

int sum(int ary[], int len)
{
    int i, summary=0;

    for(i=0; i<len; i++)
        summary += ary[i];

    return summary;
}
