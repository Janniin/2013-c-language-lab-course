#include <stdio.h>
#include <stdlib.h>

struct date {
  int month, day, year;
};

int compare_dates(struct date d1, struct date d2);
void put_date(struct date d);

int main(void)
{
    struct date d1, d2;

    scanf("%d/%d/%d", &d1.month, &d1.day, &d1.year);
    scanf("%d/%d/%d", &d2.month, &d2.day, &d2.year);

    if (compare_dates(d1, d2) < 0)
        put_date(d1);
    else
        put_date(d2);

    system("pause");
    return 0;
}

int compare_dates(struct date d1, struct date d2)
{
    if (d1.year != d2.year)
        return d1.year < d2.year ? -1 : 1;
    if (d1.month != d2.month)
        return d1.month < d2.month ? -1 : 1;
    if (d1.day != d2.day)
        return d1.day < d2.day ? -1 : 1;

    return 0;
}

void put_date(struct date d)
{
    printf("%02d/%02d/%02d\n", d.month, d.day, d.year);
}
