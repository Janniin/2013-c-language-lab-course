#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct dictionary {
    char english[10];
    char foreign[10];
};

struct dictionary dics[100000];

int read_line(char str[], int n);
int findWord(const char *FWord, int len);

int main()
{
    char readline[22], *english, *foreign;
    int i=0, ret;  // i indicate the amount of words in dictionary

    while(1) {
        read_line(readline, 22);
        if (readline[0] == '\0')
            break;
        english = strtok(readline, " ");
        foreign = strtok(NULL, " ");
        strcpy(dics[i].english, english);
        strcpy(dics[i].foreign, foreign);
        i++;
    }

    while (1) {
        read_line(readline, 11);
        if (readline[0] == '\0')
            break;
        if ((ret = findWord(readline, i)) >= 0)
            printf("%s\n", dics[ret].english);
        else
            printf("sinnian\n");
    }

    return 0;
}

int read_line(char str[], int n)
{
   int ch, i=0;

    while ((ch=getchar()) != '\n')
        if (i<n)
            str[i++] = ch;
    str[i] = '\0';
    return i;
}

int findWord(const char *FWord, int len)
{
    int ret = -1, i;
    char word[30];
    for (i=0; i<len; i++) {
        if (!strcmp(dics[i].foreign, FWord)) {
            ret = i;
            break;
        }
    }

    return ret;
}
