#include <stdio.h>
#include <stdlib.h>

#define MAX_SENTENCE_LEN 80

int main(void)
{
    int i;
    char ch, sentence[MAX_SENTENCE_LEN+1], *ptr;
    
    for (i=0; i<=MAX_SENTENCE_LEN, (ch = getchar()) != '\n'; i++) sentence[i] = ch;
    
    ptr = &sentence[--i];
    while (ptr >= &sentence[0]) putchar(*ptr--);
    putchar('\n');

    system("pause");
    return 0;
}
