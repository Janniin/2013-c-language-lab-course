#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, j, x, n, count=1, max=0;

        max = 0;
        count = 0;
        scanf("%d%d", &i, &j);
        while(i>j||i==0)   // rescan
            scanf("%d%d", &i, &j);
        printf("%d  %d", i, j);
        for(x=i; x<=j; x++) {
            count = 1;
            n = x;
            while (n!=1) {
                if (n%2==1)
                    n = 3*n+1;
                else if(n%2==0)
                    n = n/2;
                count++;
            }
            if(count>max)
                max = count;
        }
        printf("  %d\n", max);

    return 0;
}
