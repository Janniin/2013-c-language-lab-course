#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_MSG_LEN 80

int main(void)
{
    char msg[MAX_MSG_LEN], ch, *p, *q = &msg[0];

    while (q < &msg[MAX_MSG_LEN]) {
        if ((ch = getchar()) == '\n') break;
        if (isalpha(ch))
            *q++ = toupper(ch);
    }

    for (p = &msg[0], q--; p < q; p++, q--)
        if (*p != *q)
            break;

    if (p >= q)
        printf("Palindrome\n");
    else
        printf("Not a palindrome\n");

    system("pause");
    return 0;
}
